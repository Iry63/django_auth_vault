# Django Authentication Using Vault by hashicorp

This repository have been develloped for [Openlink](https://gitlab.com/ifb-elixirfr/openlink/openlink) but can still be used as a template for a Django authentication backend that authenticates against a [Vault](https://www.vaultproject.io/) service.
Using [hvac](https://github.com/hvac/hvac) a python API client for Vault.

## Installation

**Important** a vault server is required with any authentication setup.

In your django project:
```commandline
pip install git+https://gitlab.com/Iry63/django_auth_vault.git
```

### Configuration in django settings

To use the auth backend in a Django project, add `django_auth_vault.backend.VaultBackend` to `AUTHENTICATION_BACKENDS`.
```python
AUTHENTICATION_BACKENDS = [
    "django_auth_vault.backend.VaultBackend",
]
```

Then define vault parameters in settings.py
#### Example Config:
```python
AUTH_VAULT_CONFIG = {
    "VAULT_HOSTNAME": "http://127.0.0.1",
    "VAULT_PORT": ":8400",
    "AUTH_PATH_LIST": ["ldap/","userpass/"],
    "STORE_TOKEN_IN_SESSION": True,
}
```
Here is an example config with 3 required arguments `VAULT_HOSTNAME`, `VAULT_PORT`, `AUTH_PATH_LIST` list of all authentication path set up in vault.

And an optional:\
`STORE_TOKEN_IN_SESSION` which is used to store the vault session token in the django request session
at `request.session["vault_token"]`

```commandline
path "{{identity.entity.id}}/*" {
capabilities = ["create", "update", "read", "delete", "list"]
}

path "sys/mounts/{{identity.entity.id}}" {
capabilities = ["create", "update", "read", "delete", "list"]
}	
```