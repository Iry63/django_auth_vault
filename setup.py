from setuptools import find_packages, setup

__version__ = "0.1"

setup(
    name="django_auth_vault",
    version=__version__,
    description="A django backend authentication systeme using vault",
    author="Hiriart Mateo",
    packages=find_packages(),
    install_requires=[
        "django==3.0.7",
        "hvac==0.11.2"
    ]
)