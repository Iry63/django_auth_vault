import requests
from django.contrib.auth.backends import BaseBackend
from django.contrib.auth.models import User
import hvac
import django.conf
import webbrowser
import http.server
import hvac
from urllib import parse

"""
This is a Django authentication backend that authenticates against a [Vault](https://www.vaultproject.io/) service.
Using [hvac](https://github.com/hvac/hvac) a python API client for Vault.
"""


class VaultBackend(BaseBackend):
    options = getattr(django.conf.settings, "AUTH_VAULT_CONFIG", None)

    def authenticate(self, request, username=None, password=None, token=None, role=None):
        client = hvac.Client(url=self.options["VAULT_HOSTNAME"] + self.options["VAULT_PORT"])
        reponse, name = self.verification(client=client, username=username, password=password, token=token, role=role)

        if reponse:
            if self.options["STORE_TOKEN_IN_SESSION"]:
                request.session["vault_token"] = reponse["auth"]["client_token"]

            if name:
                try:
                    user = User.objects.get(username=name)
                except User.DoesNotExist:
                    user = User(username=name)
                    user.save()
                    client.sys.enable_secrets_engine(backend_type='kv', path=str(reponse["auth"]["entity_id"]))
            else:
                try:
                    user = User.objects.get(username=reponse["auth"]["entity_id"])
                except User.DoesNotExist:
                    user = User(username=reponse["auth"]["entity_id"])
                    user.save()
                    try:
                        client.sys.enable_secrets_engine(backend_type='kv', path=str(reponse["auth"]["entity_id"]))
                    except:
                        print("Error trying to create new user path in vault")
        else:
            user = None
        client.logout(revoke_token=False)
        return user

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None

    def verification(self, client, username=None, password=None, token=None, role=None):
        reponse = None
        name = None
        if username and password:
            params = {
                'password': password,
            }
            for path in self.options["AUTH_PATH_LIST"]:
                print("trying path : " + path)
                try:
                    reponse = client.login(url='/v1/auth/' + str(path) + 'login/' + username, json=params)
                    name = reponse["auth"]["metadata"]["username"]
                except:
                    print("Error login to path" + str(path) + " for username/password auth")
                if reponse:
                    break
        elif token:
            client.token = token
            try:
                reponse = client.is_authenticated()
            except:
                print("Error login with token, token not valid")
            return reponse
        elif role:
            auth_url_response = client.auth.oidc.oidc_authorization_url_request(
                role=role,
                redirect_uri='http://localhost:8250/oidc/callback'
            )
            auth_url = auth_url_response['data']['auth_url']
            if auth_url == '':
                return None

            params = parse.parse_qs(auth_url.split('?')[1])
            auth_url_nonce = params['nonce'][0]
            auth_url_state = params['state'][0]

            webbrowser.open(auth_url)
            token = login_odic_get_token()

            for path in self.options["AUTH_PATH_LIST"]:
                try:
                    print("trying path : " + path)
                    reponse = client.auth.oidc.oidc_callback(
                        code=token, path=path, nonce=auth_url_nonce, state=auth_url_state
                    )
                except:
                    print("Error login to path" + str(path) + " for oidc auth")
                if reponse:
                    break

            read_response = client.secrets.identity.read_entity(
                entity_id=reponse["auth"]["entity_id"],
            )
            name = read_response['data']['aliases'][0]['name']

        else:
            print("no credential given")
        return reponse, name


# handles the oidc callback
def login_odic_get_token():
    from http.server import BaseHTTPRequestHandler, HTTPServer

    class HttpServ(HTTPServer):
        def __init__(self, *args, **kwargs):
            HTTPServer.__init__(self, *args, **kwargs)
            self.token = None

    class AuthHandler(BaseHTTPRequestHandler):
        token = ''

        def do_GET(self):
            params = parse.parse_qs(self.path.split('?')[1])
            self.server.token = params['code'][0]
            self.send_response(200)
            self.end_headers()
            self.wfile.write(str.encode('<div>Authentication successful, you can close the browser now.</div>'))

    server_address = ('', 8250)
    httpd = HttpServ(server_address, AuthHandler)
    httpd.handle_request()

    return httpd.token
